# Run the codes using python3 command.

from tutorials.number import fun as pynumber
from tutorials.string import fun as pystring
from tutorials.list import fun as pylist
from tutorials.tuple import fun as pytuple
from tutorials.set import fun as pyset
from tutorials.variable import fun as pyvariable
from tutorials.datatype import fun as pydatatype
from tutorials.operator import fun as pyoperator
from tutorials.numbersystem import fun as pynumbersystem
from tutorials.swap import fun as pyswap

# pynumber()
# pystring()
# pylist()
# pytuple()
# pyset()
# pyvariable()
# pydatatype(-3, 20, 2)
# pyoperator()
# pynumbersystem()
pyswap()
