# Run the codes using python3 command.

num1 = 10;
num2 = 7;
newLine = '\n';

def fun(): 
  print("- - - - - - - - - - - Let's do some arithmetic operations on Python... - - - - - - - - - - -");
  print(newLine);
  print(num1, '+', num2, '=', num1 + num2, '(Addition)');
  print(num1, '-', num2, '=', num1 - num2, '(Subtraction)');
  print(num1, '/', num2, '=', num1 / num2, '(Division)');
  print(num1, '//', num2, '=', num1 // num2, '(Integer division)');
  print(num1, '%', num2, '=', num1 % num2, '(Modulus)');
  print(newLine);