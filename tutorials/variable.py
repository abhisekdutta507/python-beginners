# Run the codes using python3 command.
# Can't declare constant in Python.

var1 = 5;
var2 = 10.6;
set1 = {6, 9, 8, 9, 10};
newLine = '\n';

def fun(): 
  print("- - - - - - - - - - - Let's do some more variable operations on Python... - - - - - - - - - - -");
  print(newLine);
  print('var1 =', var1);
  print('var2 =', var2);
  print('address of var1 =', id(var1));
  print('address of var2 =', id(var2));

  print(newLine);
  print('set1 =', set1);
  print('address of set1 =', id(set1));

  print(newLine);
  var3 = 0;
  var3 = var2;
  print('assigned var2 to var3, then...');
  print('address of var1 =', id(var1));
  print('address of var2 =', id(var2));
  print('address of var3 =', id(var3));

  print(newLine);
  print('type of var1 =', type(var1));
  print('type of var2 =', type(var2));
  print('type of var3 =', type(var3));
  print('type of set1 =', type(set1));

  print(newLine);