# Run the codes using python3 command.
# Tuples are not mutable.

tuple1 = (-1, 2, -3, 14, 5);
tuple2 = (6, 9, 8, 9, 10);
newLine = '\n';

def fun(): 
  print("- - - - - - - - - - - Let's do some tuple operations on Python... - - - - - - - - - - -");
  print(newLine);
  print('tuple1', '=', tuple1);
  print('tuple2', '=', tuple2);
  print('tuple2[0]', '=', tuple2[0]);
  print('tuple2[-2]', '=', tuple2[-2]);

  # We can't override tuple elements. It will give an TypeError: 'tuple' object does not support item assignment.
  # tuple2[1] = 15;
  print('9 is present in tuple2', tuple2.count(9), 'times');
  print('First index of 9 in tuple2 is', tuple2.index(9));
  print(newLine);