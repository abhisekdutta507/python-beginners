newLine = '\n';
str1 = 'Abhisek';
str2 = 'Dutta';
str3 = 'You are using an Apple\'s MacBook Pro. ';

def fun():
  print("- - - - - - - - - - - Let's do some string operations on Python... - - - - - - - - - - -");
  print(newLine);
  print('Hi! ' + str1 + ' ' + str2 + '.');
  print(str3 * 3);
  print(newLine);
  #  0  1  2  3  4  5  6
  #  A  b  h  i  s  e  k
  # -7 -6 -5 -4 -3 -2 -1
  # Please note the indexes <= -8 and the indexes >= 7 will give an IndexError. I.e.: string index out of range.
  print('0th index of', str1, 'is', str1[0]);
  print('3rd index of', str1, 'is', str1[3]);
  print('-1th index of', str1, 'is', str1[-1]);
  print('-7th index of', str1, 'is', str1[-7]);
  print(newLine);
  # We can also select in a range of the indexes. Where both the starting and ending indexes are optional.
  print('From -7th index to 3rd index of', str1, 'is', str1[-7:3]);
  print('From -19th index to undefined index of', str1, 'is', str1[-19:]);
  print('From undefined index to 5th index of', str1, 'is', str1[:5]);
  print('From undefined index to undefined index of', str1, 'is', str1[:]);
  print(newLine);