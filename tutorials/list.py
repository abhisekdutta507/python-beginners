newLine = '\n';
array1 = [-1, 2, -3, 14, 5];
array2 = [6, 7, 8, 9, 10];

def fun(): 
  print("- - - - - - - - - - - Let's do some array operations on Python... - - - - - - - - - - -");
  print(newLine);
  # Add operator performs concatination of 2 arrays.
  print(array1, '+', array2, '=', array1 + array2);
  # But can't subtract 2 arrays using subtractact operator.
  # print(array1, '-', array2, '=', array1 - array2);
  print(newLine);
  print('Appending a new element 11 in array2...');
  array2.append(11);
  print('Array2 is now', array2);
  print(newLine);
  print('Appending multiple new elements in array2...');
  array2.extend([14, 15, 16]);
  print('Array2 is now', array2);
  print(newLine);
  print('Inserting a new element at a perticular index in array2...');
  array2.insert(6, 12);
  print('Array2 is now', array2);
  print(newLine);
  print('Poping the last element from array2...');
  array2.pop();
  print('Array2 is now', array2);
  print(newLine);
  print('Poping 3rd indexed element from array2...');
  array2.pop(3);
  print('Array2 is now', array2);
  print(newLine);
  print('Removing an element from array2...');
  array2.remove(10);
  print('Array2 is now', array2);
  print(newLine);
  print('Delete multiple numbers from array2...');
  # Deletes all the elements from index 2.
  del array2[2:];
  print('Array2 is now', array2);
  print(newLine);
  print('Min val, max val and sorted in array1...');
  print(newLine);
  print('Min in array1', min(array1), 'Max in array1', max(array1));
  array1.sort();
  print('Ascending ordered sorted array1 is', array1);
  array1.reverse();
  print('Descending ordered sorted array1 is', array1);
  print(newLine);