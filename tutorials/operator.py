# Run the codes using python3 command.
# Arithmetic, Assignment, Relational, Logical, Unary
# Arithmetic -> +, -, *, /, //, %
# Assignment -> =, +=, -=, *=, /=, //=, %=
# Unary -> eg. a = -a;
# Relational -> <, <=, >, >=, ==, !=
# Logical -> and, or, not


y = 7;
z = 1;
newLine = '\n';

def fun(): 
  print("- - - - - - - - - - - Let's do some operator oriented operations on Python... - - - - - - - - - - -");
  print(newLine);
  x = 3;
  print('x =', x);
  print('y =', y);
  x += 2;
  print('after x += 2; x =', x);
  x *= 3;
  print('after x *= 3; x =', x);
  x //= 5;
  print('after x //= 5; x =', x);
  x %= y;
  print('after x %= y; x =', x);

  # Assign multiple values at a time
  print(newLine);
  a, b = 6, 9;
  print('a =', a);
  print('b =', b);
  print('address of a', id(a));
  print('address of b', id(b));

  # Unary operators
  a = -a;
  nC = True;
  mC = -nC;
  # not() can return True or False. not is a Logical operator.
  iC = not(nC);
  w = not z;

  print('a =', a);
  print('nC =', nC);
  print('mC =', mC);
  print('iC =', iC);
  print('z =', z);
  print('w =', w);
  print('int(w) =', int(w));
  print('bool(mC) =', bool(mC));
  print('bool(z) =', bool(z));

  # Unary operators
  print(newLine);
  a = 5;
  b = 6;
  print('a =', a);
  print('b =', b);
  print('a < b', a < b);
  print('a > b', a > b);
  print('a == b', a == b);
  print('a <= b', a <= b);
  print('a >= b', a >= b);
  print('a != b', a != b);

  # Logical operators
  print(newLine);
  a = 5;
  b = 4;
  c = 0;
  print('a =', a);
  print('b =', b);
  print('c =', c);
  print('a < 8 and b < 5 =', a < 8 and b < 5);
  print('a < 8 and b < 2 =', a < 8 and b < 2);
  print('a < 8 or b < 5 =', a < 8 or b < 5);
  print('a < 8 or b < 2 =', a < 8 or b < 2);
  print('not a =', not a);
  print('not c =', not c);
