# Run the codes using python3 command.
# Decimal (base - 10) [0, 1, ..., 9]
# Binary (base - 2) [0, 1]
# Octal (base - 8) [0, 1, ..., 7]
# Hexadecimal (base - 16) [0, 1, ..., 7, 8, 9, A, B, C, D, E, F]

newLine = '\n';

def fun():
  print("- - - - - - - - - - - Let's do some number system oriented operations on Python... - - - - - - - - - - -");
  print(newLine);
  x = 10;
  y = 25
  print('x =', x);
  print('y =', y);
  print('bin(25) =', bin(25));
  print('bin(y) =', bin(y));
  print('oct(x) =', bin(x));
  print('hex(x) =', hex(x));
  print('decimal value of binary 1010 =', 0b1010);
  print('decimal value of hexadecimal a =', 0xa);