# Run the codes using python3 command.
# Sets do not contain repeated values.
# It contains random values. No sequence is maintained in a set.
# Set uses memory hashing to access the values more faster than a list.

set1 = {-1, 2, -3, 14, 5, 2};
set2 = {6, 9, 8, 9, 10};
newLine = '\n';

def fun(): 
  print("- - - - - - - - - - - Let's do some set operations on Python... - - - - - - - - - - -");
  print(newLine);
  print('set1 =', set1);
  print('set2 =', set2);
  # Their is no support for indexing. It gives an TypeError: 'set' object is not subscriptable.
  # print('set2[1]', '=', set2[1]);
  set1.add(15);
  print('set1.add(15) & now set1 =', set1);
  set1.add(15);
  print('set1.add(14) & now set1 =', set1, 'nothing changes. Sets do not contain repeated values.');

  # Remove generates an error when the element is not present in a set.
  set1.remove(-1);
  print('set1.remove(-1) & now set1 =', set1);

  # Discard doesn't generate any error when the element is not present in a set.
  set1.discard(190);
  print('set1.discard(190) & now set1 =', set1);

  # A set can be updated with the elements of another set
  set1.update(set2);
  print('set1.update(set2) & now set1 =', set1);

  # Pop removes the 1st element from a set
  set1.pop();
  set1.pop();
  print('set1.pop(); & set1.pop(); then set1 =', set1);

  print('set1.intersection(set2) =', set1.intersection(set2));

  print('set1.union(set2) =', set1.union(set2));

  print(newLine);
