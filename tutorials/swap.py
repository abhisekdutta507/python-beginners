# Run the codes using python3 command.

newLine = '\n';

def fun():
  print("- - - - - - - - - - - Let's do some number swapping operations on Python... - - - - - - - - - - -");
  print(newLine);

  a = 10;
  b = 20;
  print('a, b =', a, b);
  
  # In Python way...
  a, b = b, a;
  print('a, b =', a, b);

  # Other possible way 1...
  a = a + b;
  b = a - b;
  a = a - b;
  print('a, b =', a, b);

  # Other possible way 2...
  a = a ^ b;
  b = a ^ b;
  a = a ^ b;
  print('a, b =', a, b);

  # Other possible way 3...
  c = a;
  a = b;
  b = c;
  print('a, b =', a, b);