# Run the codes using python3 command.
# None, Numeric, Bool, List, Tuple, Set, String, Range, Dictionary/Map
# Numeric -> int(3), float (2.5), complex
# String -> can contain RAW strings. A RAW string always looks like r'type your string here...'

var1 = 3
var2 = 2.5
set1 = {6, 9, 8, 9, 10}
str1 = 'Abhisek'
complex1 = 6+9j
bool1 = True
dictionary1 = {
    'fname': 'Abhisek',
    'lname': 'Dutta',
    'phone': '9876543210',
    'email': 'abhisekdutta@gmail.com'
}
newLine = '\n'


def fun(r1, r2, dif):
    print("- - - - - - - - - - - Let's do some data type operations on Python... - - - - - - - - - - -")
    print(newLine)
    print('type of var1', type(var1))
    print('type of var2', type(var2))
    print('type of set1', type(set1))
    print('type of str1', type(str1))
    print('type of complex1', type(complex1))
    print(r'type of \n', type(newLine))
    print(r'type of bool1', type(bool1))

    # Numeric type conversions
    print(newLine)
    var3 = float(var1)
    var4 = int(var2)
    print('var3 =', var3)
    print('var4 =', var4)
    print('type of var3', type(var3))
    print('type of var4', type(var4))

    # Generating complex from 2 numbers. Complex function has only the 1st parameter as required parameter. By default the 2nd parameter's value is zero.
    print(newLine)
    complex2 = complex(var2, var3)
    print('complex2 =', complex2)
    print('type of complex2', type(complex2))

    # Use of range in Python
    print(newLine)
    # only r2 is required.
    # when passed only 1 parameter it is pretended as r2. where r1 is 0 by default.
    # dif is 1 by default and must not be zero.
    range1 = range(r1, r2, dif)
    print('range1 =', range1)
    print('type of range1', type(range1))

    # convert range into a list
    list1 = list(range1)
    print('list1 =', list1)

    # Let's talk about dictionaries and maps
    print('dictionary1 =', dictionary1)
    print('type of dictionary1', type(dictionary1))
    print('keys of dictionary1 are', dictionary1.keys())
    print('values of dictionary1 are', dictionary1.values())
    print('fname in dictionary1 is', dictionary1.get('fname'))
    print('lname in dictionary1 is', dictionary1['lname'])
