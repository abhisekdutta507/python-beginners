from pathlib import Path
from ecommerce.delivery import vendor
# custom package
from ecommerce.shopping.cart import add_to_cart
from ecommerce.product import product_details
# press Ctrl + Space to get reference
# from sales import *
# import sales
# sales.calculate_shipping()
# custom module
from sales import calculate_shipping, calculate_tax
from collections import namedtuple
from abc import ABC, abstractmethod
from pprint import pprint
from sys import getsizeof, path
from array import array
from collections import deque
import math
# import tutorials.primitive_types
print("*" * 10)


def debug_multiply(*numbers):  # variable arguments as tupples
    total = 1
    for number in numbers:
        total *= number
    return total


print("Start debugging")
print(debug_multiply(1, 2, 3))


def fizz_buzz(input):
    fizz = input % 3 == 0
    buzz = input % 5 == 0

    if fizz and buzz:
        return "FizzBuzz"
    if fizz:
        return "Fizz"
    if buzz:
        return "Buzz"
    return input


# positional argument
print(fizz_buzz(3))
print(fizz_buzz(5))
print(fizz_buzz(15))
print(fizz_buzz(26))

# lists
letters = ["a", "b", "c", "d", "e", "f"]
# 2D lists
matrix = [[0, 1], [2, 3]]
zeros = [0] * 50
combined = zeros + letters

print(combined)

numbers = list(range(20))
print(numbers)

print(list("Hello world"))

print(len("Hello"))
print(len(matrix))

letters[0] = "A"
print(letters)
print(letters[1:-1])

# prints from index 1 with distance of 2 indexes
print(numbers[1::2])
# default first index is 0
print(numbers[::3])
# prints reverse list
print(numbers[::-1])

# list unpacking and packing
first, second, *other, last = numbers
print(first, second, other, last)

items = (0, "a")
first, second = items
print(first, second)

letters = list("Abhisek Dutta")
for index, letter in enumerate(letters):
    print(index, letter)


letters = list("abcdc")

# Add
letters.append("e")
print(letters)
letters.insert(3, "d")
print(letters)

# Remove
letters.pop()
print(letters)
letters.pop(0)
print(letters)
# removes the first ocurrence of c
letters.remove("c")
print(letters)
# remove multiple using del
del letters[1:3]
print(letters)
# removes all the objects from list
letters.clear()
print(letters)

letters = ["a", "b", "c"]
print(letter.count("d"))

if "d" in letter:
    # It can crush into ValueError if the item is not present in the list
    print(letters.index("d"))

numbers = [2, 8, -9, 5, 10, 9]

# sorts the actual array
numbers.sort(reverse=True)
print(numbers)

numbers = [2, 8, -9, 5, 10, 9]

# creates a different array with sorted numbers
sortedNumbers = sorted(numbers, reverse=True)
print(sortedNumbers)


items = [
    ("Product 1", 400),
    ("Product 2", 140),
    ("Product 3", 1200),
    ("Product 4", 700)
]


def sort_item(item):
    return item[1]


# keyword arguments
items.sort(key=sort_item)
print(items)

# lambda function
items.sort(key=lambda item: item[1], reverse=True)
print(items)

items = [
    ("Product 1", 400),
    ("Product 2", 140),
    ("Product 3", 1200),
    ("Product 4", 700)
]

prices1 = list(map(lambda item: item[1], items))
print(prices1)

# comprehension
prices2 = [item[1] for item in items]
print(prices2)

filtered1 = list(filter(lambda item: item[1] <= 500, items))
print(filtered1)

filtered2 = [item for item in items if item[1] <= 500]
print(filtered2)

# zip function
list1 = [1, 2, 3, 4]
list2 = [10, 20, 30, 40]
print([item for item in zip(list1, list2)])

# stack
browsing_session = []
browsing_session.append(1)
browsing_session.append(2)
print(browsing_session)

poped = browsing_session.pop()
print(poped, browsing_session)

poped = browsing_session.pop()
print(poped, browsing_session)

if not browsing_session:
    print("Stack is empty")

# queue
queue = deque([])
queue.append(1)
queue.append(2)
queue.append(3)

poped = queue.popleft()
print(poped, queue)

poped = queue.popleft()
print(poped, queue)

poped = queue.popleft()
print(poped, queue)

if not queue:
    print("Queue is empty")

# tuple or readonly object
point1 = 1, 2
point2 = 1,
point3 = ()
point4 = (1, 2, 3)
point5 = tuple([1, 2, 3, 4])
point6 = tuple("Abhisek Dutta")

# tuple unpacking
first, *other, last = point5
print(first, other, last)

if 10 in point3:
    print("Exists")
else:
    print("Do not exists")

# swap 2 variables
x = 10
y = 12

x = x + y
y = x - y
x = x - y

print(f"x: {x}, y: {y}")

# swap variables in Python tuple way
x = 5
y = 6
x, y = y, x

print(f"x: {x}, y: {y}")

# array typecodes. https://docs.python.org/3/library/array.html
# similar to list but used for faster execution. there is no need to use an array if we do not face any performance issue.
numbers = array("i", [5, 6, 7])
print(type(numbers))

# pass the index that is to be poped
numbers.pop(1)
print(numbers)

# numbers.append(1.0)  # TypeError: integer argument expected, got float

# set is an unordered collection of unique items. we can not have duplicates and items in set are unordered and not indexed.
first = set([1, 2, 3, 4, 5])
second = {1, 5, 5, 6}

print(first, second)

# union of sets
print(first | second)
# intersection of sets
print(first & second)
# difference of sets
print(first - second)
# symetric difference between 2 sets
print(first ^ second)  # {2, 3, 4, 6} # caret symbol

# dictionaries
point1 = {
    "x": 1,
    "y": 2
}
point2 = dict(x=1, y=2)
point2["z"] = 3

if "a" in point2:
    print(point2["a"])

# 1st argument is the key that we want to fetch. 2nd argument is the default value when the key is missing.
target = point2.get("a", 0)
print(point2, target)

del point2["z"]
print(point1, point2)

for key in point1:
    print(key, point1[key])

for key, value in point1.items():
    print(key, value)

# comprehension
# list
# set
# dictionary
table1 = []
for x in range(10):
    table1.append(x * 2)

table2 = [x * 2 for x in range(10)]

print(table1, table2)

set1 = set()
print(type(set1), set1)
for x in range(10):
    set1.add(x * 2)

set2 = {x * 2 for x in range(10)}

print(set1, set2)

dictionary1 = {}
print(type(dictionary1), dictionary1)

for x in range(10):
    dictionary1[x] = x * 2
dictionary2 = {x: x * 2 for x in range(10)}

print(dictionary1, dictionary2)

# normal list
values = [x * 2 for x in range(200)]
print("list: ", getsizeof(values))

# generator expression
values = (x * 2 for x in range(200))
print("gen: ", values, "size: ", getsizeof(values))
# for value in values:
#     print(value)

# extreme unpacking
# can work with any iterable
values = list(range(5))
values = [*range(5), *"abc"]
print(values)

first = {"x": 1}
second = {"x": 10, "y": 20}
combined = {**first, **second, "z": 30}
print(combined)

# find the most repeated character in the given sentence
sentence = "This is a common interview question"
broken = [*sentence]
char_frequency = {x: broken.count(x) for x in set(sentence) if x != " "}
char_frequency_sorted = sorted(
    char_frequency.items(),
    key=lambda item: item[1],
    reverse=True)
# pprint(char_frequency_sorted, width=1)
print(char_frequency_sorted[0])

# Multiple Inheritance

# Inheritance

# named tuples
# data classes
NamedPoint = namedtuple("NamedPoint", ["x", "y"])
named1 = NamedPoint(1, 2)
named2 = NamedPoint(1, 2)
print(named1 == named2)

named1 = NamedPoint(13, 2)
print(named1 == named2)

# imported from custom module
calculate_shipping()

# imported from custom package
print(product_details(id="abv8ihdf33sj21"))

print(path)


print(add_to_cart(id="abv8ihdf33sj21"))

# the dir function
print(dir(vendor))
# magic attributes
print(vendor.__doc__)
print(vendor.__file__)
print(vendor.__name__)
print(vendor.__package__)

# __main__ is default when it is the main executable file
print("App initiated", __name__)
# raw string
# no escape character
