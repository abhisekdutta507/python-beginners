import math
newLine = '\n'

print(f" - - - Integer & Float - - - ")
x = 1  # integer number
unit_price = 3.99  # floating point number / float number
print(f"x = {x}")
print(f"unit_price = {unit_price}", newLine)

# boolean, unlike C, C++, Java and JavaScript True starts with a capital letter
print(f" - - - Boolean - - - ")
is_published = False
is_printed = True
print(f"is_published = {is_published}")
print(f"is_printed = {is_printed}", newLine)

print(f" - - - Assigning Variables - - - ")
me = 19
you = me  # a new variable is created
you = 8
print(f"me = {me}, you = {you}", newLine)  # 19 8

print(f" - - - String Operations - - - ")
name = "Abhisek Dutta"  # string values
FIRST_NAME = "Abhisek"
print(f"FIRST_NAME = {FIRST_NAME}, name = {name}")
print(f"name[0] = {name[0]}")  # A
print(f"name[-1] = {name[-1]}")  # a
print(f"name[0:3] = {name[0:3]}")  # Abh
print(f"name[0:] = {name[0:]}")  # Abhisek Dutta
print(f"name[:4] = {name[:4]}")  # Abhi
print(f"name[:] = {name[:]}")  # Abhisek Dutta
print(f"name[-2:] = {name[-2:]}")  # ta
print(f"name[1:-1] = {name[1:-1]}", newLine)  # bhisek Dutt

print(f" - - - Multiline String - - - ")
# multiline string
message = """
Hi Arindam,

What's up bro.

Blah blah blah...
"""
print(f"message  = {message}")
print(f"len(message) = {len(message)}", newLine)  # 48

print(f" - - - Escape Sequence - - - ")
# \"
# \'
# \\
# \n
course = "Python \"Programming"
print(r"Python \"Programming =", course, newLine)

print(" - - - Raw String - - - ")
print(r'r"..." is used to print raw strings', newLine)

print(" - - - Formatted String - - - ")
first = "Abhisek"
last = "Dutta"
full = f"{first} {last}"
Full = F"Mr. {first} {last}"
print(f"full = {full}, Full = {Full}", newLine)

print(" - - - String Methods - - - ")
name = "abhisek dutta"
print(f"name = {name}")
print(f"name.capitalize() = {name.capitalize()}")  # Abhisek dutta
print(f"name.upper() = {name.upper()}")  # ABHISEK DUTTA
print(f"name.title() = {name.title()}", newLine)  # Abhisek Dutta

string_with_spaces = "  python programmers are cool    "
print(f"string_with_spaces = {string_with_spaces}")
# trim spaces from a string
print(f"string_with_spaces.strip() = {string_with_spaces.strip()}. END")
# trim spaces from the end of a string
print(f"string_with_spaces.rstrip() = {string_with_spaces.rstrip()}. END")
# returns the index in string
# 2
print(f'string_with_spaces.find("pyt") = {string_with_spaces.find("pyt")}')
# returns boolean value
# finds Pro if presents in string
# False
print(f'"Pro" in string_with_spaces = {"Pro" in string_with_spaces}')
# returns boolean value
# finds Pro if not presents in string
print(
    f'"swift" not in string_with_spaces = {"swift" not in string_with_spaces}')
# replace all text matches in the string
#   jython jrogrammers are cool
print(
    f'string_with_spaces.replace("p", "j") = {string_with_spaces.replace("p", "j")}', newLine)

print(" - - - Numbers - - - ")
num = 1
num = 1.9
num = 9 + 3j  # complex numbers in python

# floating point division
# 3.3333333333333335
print(f"10 / 3 = {10 / 3}")
# integer division
# 3
print(f"10 // 3 = {10 // 3}")
# modulas
# 1
print(f"10 % 3 = {10 % 3}")
# exponent or power
# 1000
print(f"10 ** 3 = {10 ** 3}", newLine)
# print("1" + 2) # TypeError: can only concatenate str (not "int") to str

number = 5
# augmented assignment operator
# 8
number += 3

# positive numbers
p1 = 3.4
p2 = 1.5
# negetive numbers
n1 = -2.9
n2 = -7.4
print(f"p1 = {p1}, p2 = {p2}")
print(f"round(p1) = {round(p1)}")  # 3
print(f"round(p2) = {round(p2)}", newLine)  # 2
print(f"n1 = {n1}, n2 = {n2}")
print(f"round(n1) = {round(n1)}")  # -3
print(f"round(n2) = {round(n2)}", newLine)  # -7
# absolute numbers
# always positive
print(f"n1 = {n1}, p1 = {p1}")
print(f"abs(n1), abs(p1) = {abs(n1)}, {abs(p1)}", newLine)  # 2.9, 3.4

# ceiling and floor values
print(f"math.ceil(2.1) = {math.ceil(2.1)}")  # 3
print(f"math.floor(4.7) = {math.floor(4.7)}", newLine)  # 4

# print(math.isinf(10 / 0))  # ZeroDivisionError: division by zero

print(" - - - Type Conversions - - - ")
print(f"type(5) = {type(5)}")  # <class 'int'>
print(f"type(5.5) = {type(5.5)}")  # <class 'float'>
print(f'type("Hello") = {type("Hello")}')  # <class 'str'>
print(f"type(True) = {type(True)}", newLine)  # <class 'bool'>

# comples types
print(f"type(2 + 3j) = {type(2 + 3j)}")  # <class 'complex'>
print(f"type(range(3)) = {type(range(3))}", newLine)  # <class 'range'>

# Falsy
# ""
# 0
# None # similar output as print()
# Type Conversion using
# int(x)
# float(x)
# bool(x)
# str(x)
print(f"bool(None) = {bool(None)}", newLine)

print(" - - - ASCII Comparison - - - ")
print(f'"bag" > "apple" = {"bag" > "apple"}')  # True
print(f'"Bag" > "apple" = {"Bag" > "apple"}')  # False
# function ord only accepts string
# 66, 98, 97, 48
print(
    f'ord("B"), ord("b"), ord("a"), ord("0") = {ord("B")}, {ord("b")}, {ord("a")}, {ord("0")}', newLine)
