from abc import ABC, abstractmethod
newLine = "\n"

# Classes in Python
#
#
#
#
print(" - - - Classes - user defined datatypes - - - ")


class Point:
    # class level attributes
    default_color = "red"

    # magic method
    # constructor
    # instance level attributes
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self):
        print(f"(x, y) = ({self.x}, {self.y})")


point = Point(1, 3)
print(f"point = {point}")
print(f"type(point) = {type(point)}")  # <class '__main__.Point'>
print(f"isinstance(point, Point) = {isinstance(point, Point)}")  # True
point.draw()

another = Point(5, 8)
another.draw()
print(newLine)

# Class level attribute in Python
#
#
#
#
print(" - - - Class Level Attribute - - - ")
print(f"point.default_color = {point.default_color}")
# access class level attributes
Point.default_color = "white"
print(
    f"Point.default_color = {Point.default_color}, another.default_color = {another.default_color}")
# override the class level attribute for an instance
another.default_color = "yellow"
print(
    f"Point.default_color = {Point.default_color}, another.default_color = {another.default_color}", newLine)


# Magic method
#
#
#
#
print(" - - - Magic Methods of a Class - - - ")


class NewPoint:
    # magic method
    # constructor
    # instance level attributes
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # class methods
    @classmethod
    def zero(cls):
        return cls(0, 0)

    def draw(self):
        print(f"(x, y) = ({self.x}, {self.y})")

    # magic methods
    # convert object into string
    def __str__(self):
        return f"(x, y) = ({self.x}, {self.y})"

    # check equality
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    # check greater than and lesser than
    def __gt__(self, other):
        return self.x > other.x and self.y > other.y

    # add 2 objects
    def __add__(self, other):
        return NewPoint(self.x + other.x, self.y + other.y)


point = NewPoint.zero()
point.draw()

# print the object as string
print(point)  # (x, y) = (0, 0)
print(str(point))  # (x, y) = (0, 0)
print(newLine)

# Compare 2 objects in Python
#
#
#
#
print(" - - - Compare 2 objects - - - ")
next_point = NewPoint.zero()
print(f"next_point = {next_point}")  # (x, y) = (0, 0)
# compare 2 objects
print(f"next_point == point = {next_point == point}")  # True

np1 = NewPoint(1, 2)
np2 = NewPoint(3, 5)
# np1 = (x, y) = (1, 2), np2 = (x, y) = (3, 5)
print(f"np1 = {np1}, np2 = {np2}")
# compare 2 objects
print(f"np2 > np1 = {np2 > np1}")  # np2 > np1 = True

addition = np1 + np2
print(f"np1 + np2 = {addition}", newLine)  # np1 + np2 = (x, y) = (4, 7)

# Private attributes in Python
#
#
#
#
print(" - - - Private Attributes - - - ")


class TagCloud:
    # magic methods
    # constructor
    def __init__(self):
        # private attribute
        self.__tags = {}

    # magic methods
    def add(self, tag):
        self.__tags[tag.lower()] = self.__tags.get(tag.lower(), 0) + 1

    # get the items
    def __getitem__(self, tag):
        return self.__tags.get(tag.lower(), 0)

    def __setitem__(self, tag, count):
        self.__tags[tag.lower()] = count

    # magic methods
    def __len__(self):
        return len(self.__tags)

    # magic methods
    def __iter__(self):
        return iter(self.__tags)


cloud = TagCloud()
print(f'cloud["Python"] = {cloud["Python"]}')
cloud.add("Python")
cloud.add("python")
cloud.add("python")
# private members can no be accessed from outside
# print(cloud.__tags)
print(f'cloud = {cloud}')

cloud["Python"] = 10
# still can access private members
print(f'Hidden items {cloud.__dict__}')

print(f"len(cloud) = {len(cloud)}")

for key in cloud:
    print(f"key = {key}")
print(f"Done!", newLine)

# Getters and Setters in Python
# Classes
# 11. Properties (7:30)
#
#
print(f" - - - Getters and Setters - - - ")


class Product:  # getters and setters
    def __init__(self, price):
        self.price = price

    @property
    def price(self):
        return self.__price

    @price.setter
    def price(self, value):
        if value < 0:
            raise ValueError("Price can not be negative.")
        self.__price = value

    # price = property(get_price, set_price)


product = Product(50)
print(f"product.price = {product.price}", newLine)

# Inheritance in Python
# Classes
# 12. Inheritance (4:23)
#
#
print(" - - - Inheritance - - - ")


class Animal:
    def __init__(self):
        self.age = 1

    def eat(self):
        print("eat")

# Animal class is the Base class
# Mammal class is the Derived class or Sub class


class Mammal(Animal):
    # Method overriding
    def __init__(self):
        super().__init__()
        self.age = 2
        self.weight = 10

    def walk(self):
        print("walk")


class Fish(Animal):
    def swim(self):
        print("swim")


m = Mammal()
m.eat()
print(f"age = {m.age}")
print(f"isinstance(m, Mammal) = {isinstance(m, Mammal)}")
print(f"isinstance(m, Animal) = {isinstance(m, Animal)}")

# object class
print(f"isinstance(m, object) = {isinstance(m, object)}")

# is derived class
print(f"issubclass(Mammal, Animal) = {issubclass(Mammal, Animal)}", newLine)

# Multiple inheritance
# Classes
# 16. Multiple inheritance (3:22)
#
#
print(" - - - Multiple Inheritance - - - ")


class Employee:
    def greet(self):
        print("Employee greet")


class Person:
    def greet(self):
        print("Person greet")


class Manager(Employee, Person):
    pass


manager = Manager()
# Employee class is inherited first
manager.greet()  # Employee greet
print(newLine)

# Abstract base class & Abstract method
#
#
#
#
print(" - - - Abstract base class & Abstract method - - - ")


class InvalidOperationError(Exception):
    pass


class Stream(ABC):
    def __init__(self):
        self.opened = False

    def open(self):
        if self.opened:
            raise InvalidOperationError("Stream is already opened")
        self.opened = True

    def close(self):
        if not self.opened:
            raise InvalidOperationError("Stream is closed")
        self.opened = False

    @abstractmethod
    def read(self):
        pass


class FileStream(Stream):
    # must create the read method
    # abstract method
    def read(self):
        print("Reading data from a file")


class NetworkStream(Stream):
    # must create the read method
    # abstract method
    def read(self):
        print("Reading data from a network")


def read(controls):  # duck typing
    for control in controls:
        control.read()


file = FileStream()
network = NetworkStream()
# polymorphism in Python
read([file, network])
print(newLine)

# Extend Built In Types
#
#
#
#
print(" - - - Extend Built In Types - - - ")


class Text(str):  # extend built in types
    def duplicate(self):
        return self + self

    def lower(self):
        print("Method is overriden")
        return super().lower()


text = Text("Good Evening")
print(f"text.duplicate() = {text.duplicate()}")
print(f"text.lower() = {text.lower()}")
