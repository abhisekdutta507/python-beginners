newLine = '\n'
# can not run using code runner. because output window in readonly
# Type Conversion using
# int(x)
# float(x)
# bool(x)
# str(x)
print(" - - - Input values from Command Line Interface - - - ")
x = input("x: ")
print(f"type(x) = {type(x)}")
y = int(x) + 1
print(f"x: {x}, y: {y}", newLine)
