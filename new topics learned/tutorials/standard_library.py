from zipfile import ZipFile
import shutil
from time import ctime
from pathlib import Path
newLine = "\n"

# - - - - - Working with Paths - - - - -
#
#
#
print(" - - - Path Object - - - ")
# Path object
# Raw string
dic = {
    "p1": Path(r"C:\Program Files\Microsoft"),
    "p2": Path(
        "/home/acer/Projects/Learning Materials/Python/python-beginners/new topics learned"),
    "p3": Path(),
    "p4": Path("ecommerce/__init__.py"),
    "p5": Path() / "ecommerce" / "__init__.py",
    "p6": Path.home()
}

print(f"dic = {dic}")
print(f'dic["p1"].exists() = {dic["p1"].exists()}')  # False
print(f'dic["p2"].is_dir() = {dic["p2"].is_dir()}')  # True
print(f'dic["p4"].is_file() = {dic["p4"].is_file()}')  # True
print(f'dic["p5"].name = {dic["p5"].name}')  # __init__.py
print(f'dic["p5"].stem = {dic["p5"].stem}')  # __init__
print(f'dic["p5"].suffix = {dic["p5"].suffix}')  # .py
print(f'dic["p5"].parent = {dic["p5"].parent}', newLine)  # ecommerce

next_path = dic["p5"].with_name("README.md")
next_suffix = dic["p4"].with_suffix(".env")
# ecommerce/README.md ecommerce/__init__.env
print(next_path, next_suffix)
# /home/acer/Projects/Learning Materials/Python/python-beginners/new topics learned/tutorials/ecommerce/README.md
print(next_path.absolute(), newLine)

# - - - - - Deal with your directories - - - - -
#
#
#
print(" - - - Deal with a directory - - - ")
# Working with Path
next_path = Path("ecommerce")
# make the directory
# next_path.mkdir()
# remove the directory
# next_path.rmdir()
# rename the directory
# next_path.rename("sales")
print(newLine)

# - - - - - Iterate Files and Folders - - - - -
#
#
#
print(" - - - Iterate Files and Folders in a Directory - - - ")
# reads all the files and directories one after another
# do not work recusively only 1 level files and folders
files_and_folders = next_path.iterdir()
# <generator object Path.iterdir at 0x7ff3e3f0c350>
print(f"next_path = {next_path}")
print(f"next_path.iterdir() = {files_and_folders}")
for f in files_and_folders:
    print(f)
print("Done", newLine)

files_and_folders_array = [f for f in files_and_folders]
# generator functions can be iterated only once
print(f"files_and_folders_array = {files_and_folders_array}", newLine)  # []

# - - - - - Iterate folders only - - - - -
# - - - - - Filter the folders from iteratable - - - - -
#
#
print(" - - - Iterate folders only - - - ")
files_and_folders_array = [f for f in next_path.iterdir() if f.is_dir()]
# generator functions can be iterated only once
# [PosixPath('ecommerce/cart'), PosixPath('ecommerce/shipping')]
print(f"files_and_folders_array = {files_and_folders_array}", newLine)

# - - - - - Search for files with expression - - - - -
#
#
#
print(" - - - Search files using expression - - - ")
print(f"next_path = {next_path}")
files1 = [f for f in next_path.glob("*.py")]
# [PosixPath('ecommerce/__init__.py')]
print(f"files1 = {files1}")

# recursive
files2 = [f for f in next_path.glob("**/*")]
files3 = [f for f in next_path.rglob("*.py")]
files4 = [f for f in next_path.rglob("**/*.py")]
# [PosixPath('ecommerce/cart'), PosixPath('ecommerce/shipping'), PosixPath('ecommerce/__init__.py'), PosixPath('ecommerce/cart/cart.py'), PosixPath('ecommerce/cart/__init__.py'), PosixPath('ecommerce/shipping/__init__.py')]
print(f"files2 = {files2}")
# [PosixPath('ecommerce/__init__.py'), PosixPath('ecommerce/cart/cart.py'), PosixPath('ecommerce/cart/__init__.py'), PosixPath('ecommerce/shipping/__init__.py')]
print(f"files3 = {files3}")
print(f"files4 = {files4}", newLine)

# - - - - - Working with files - - - - -
#
#
#
print(" - - - Working with Files - - - ")


def working_with_files():
    path = Path("ecommerce/files/info.txt")
    print(path.exists())  # True

    # moves and renames the file from one place to another
    # print(path.rename("ecommerce/files/init.txt"))  # ecommerce/init.txt

    # deletes the file
    # print(path.unlink())  # None

    # returns the file details
    stat = path.stat()
    # os.stat_result(st_mode=33204, st_ino=8913411, st_dev=2051, st_nlink=1, st_uid=1000, st_gid=1000, st_size=0, st_atime=1601825955, st_mtime=1601825955, st_ctime=1601825955)
    # st_size is size in byte
    # st_atime is last access time
    # st_mtime is last modify time
    # st_ctime is creation time
    print(stat)

    # gives your local time to you
    print(ctime(stat.st_ctime))

    # gives you all the binary data present in the file
    print(path.read_bytes())

    # converts the content of the file in string
    print(path.read_text())

    # overrides the file content with current data
    path.write_text("Hello ... \nHow are you doing guys!")
    # path.write_bytes()

    target = Path("ecommerce/files/init.txt")
    # copy file from one location to another
    # if the file name is already exists then the file will be overriden
    shutil.copy(path, target)
    print(newLine)


# working_with_files()

# - - - - - Working with ZIP files - - - - -
#
#
#
print(" - - - Working with zip files - - - ")


def working_with_zip_files():
    # writing content in a zip file
    with ZipFile("files/files.zip", "w") as zip:
        for path in Path("ecommerce").glob("**/*"):
            zip.write(path)

    # reading from a zip file
    with ZipFile("files/files.zip") as zip:
        print(zip.namelist())
        info = zip.getinfo("ecommerce/cart/cart.py")
        print(info.file_size)
        print(info.compress_size)


working_with_zip_files()
