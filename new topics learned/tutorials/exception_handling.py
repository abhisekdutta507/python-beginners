from timeit import timeit
newLine = "\n"

print(" - - - Exception Handling - - - ")
try:
    age = int(input("Age: "))
    xfactor = 10 / age
except ValueError as ex:
    print("Error:", type(ex), ex)
except ZeroDivisionError as ex:
    print("Error:", type(ex), ex)
else:
    print("All went well")
print("Execution continues...", newLine)

print(" - - - Exception handling with less codes - - - ")
try:
    age_exception = int(input("Age: "))
    xfactor = 10 / age_exception
except (ValueError, ZeroDivisionError) as ex:
    print("Error: ", type(ex), ex)
else:
    print("All went well")
finally:
    print("Execution ended")
print("Execution continues... with input", age_exception, newLine)

print(" - - - auto file close using with statement - - - ")
try:
    with open("app.py") as app, open("next.txt") as next_txt:
        print("Files opened", app, next_txt.readlines())
except FileNotFoundError as ex:
    print("Error: ", type(ex), ex)
else:
    print("All went well")
print("Execution continues...", newLine)

print(" - - - Raising an exception - - - ")


def calculate_xfactor(age):
    if age <= 0:
        raise ValueError("Age can not be 0 or less")
    return 10 / age


age = -1
print(f"age = {age}")
try:
    calculate_xfactor(age)
except ValueError as ex:
    print(ex)
print("Execution continues...", newLine)

print(" - - - Execution time comparison - - - ")
code1 = """
def calculate_xfactor(age):
    if age <= 0:
        raise ValueError("Age can not be 0 or less")
    return 10 / age


try:
    calculate_xfactor(-1)
except ValueError as ex:
    pass
"""

code2 = """
def calculate_xfactor(age):
    if age <= 0:
        return None
    return 10 / age


xfactor = calculate_xfactor(-1)
if xfactor == None:
    pass
"""
print("Code 1", timeit(code1, number=100000))  # 0.06702040299933287
print("Code 2", timeit(code2, number=100000), newLine)  # 0.031545026000458165
