newLine = '\n'
print(" - - - Conditional Statement - - - ")
# conditional statement
temperature = "30"
print(f"temperature = {temperature}")
if int(temperature) > 30:
    print("It's warm")
    print("Drink water")
elif int(temperature) > 20:
    print("It's normal")
else:
    print("It's nice")
print("Done", newLine)

print(" - - - Ternary Operator - - - ")
# ternary operator
age = 22
print(f"age = {age}")
message = "Eligible" if age >= 18 else "Not eligible"
print(message, newLine)  # Eligible

print(" - - - Short Circuit Logical Operator - - - ")
# short circuit logical operators
# and
# or
# not
high_income = True
good_credit = True
student = False
print(
    f"high_income = {high_income}, good_credit = {good_credit}, student = {student}")
message = "Eligible" if high_income and good_credit and not student else "Not eligible"
print(message, newLine)  # Eligible

print(" - - - Chaining Comparison Operators - - - ")
# chaining comparison operators
age = 60
print(f"age = {age}")
if 18 <= age < 60:
    print("Eligible")
else:
    print("Not eligible")
print("Done", newLine)

print(" - - - For Loop - - - ")
# for loop. by default range starts from 0
# range takes 1 parameter by default. i.e. stop
# range can take 3 parameters. i.e. start, stop and step
# Attempt 0 .
# Attempt 2 ...
# Attempt 4 .....
for number in range(0, 5, 2):
    # formatted string
    print(f"Attempt {number} {(number) * '.'}")
print("Done", newLine)

print(" - - - For Else - - - ")
# for..else
# if break is not executed then else will be executed
# Attempt
# Successfull
success = True
for number in range(3):
    print("Attempt")
    if success:
        print("Successfull")
        break
else:
    # executed only if loop was not broken
    print("Attempted 3 times and failed")
print("Done", newLine)

print(" - - - Nested For Loop - - - ")
# nested loop
# (0, 0)
# (0, 1)
# (0, 2)
# (1, 0)
# (1, 1)
# (1, 2)
# (2, 0)
# (2, 1)
# (2, 2)
for x in range(3):
    for y in range(3):
        # formatted string
        print(f"{x, y}")
print("Done", newLine)

print(" - - - Iterate Range - - - ")
# range is iterables
# 0
# 1
# 2
for x in range(3):
    print(x)
print("Done", newLine)

print(" - - - Iterate String - - - ")
# range is iterables
# P
# y
# t
# h
# o
# n
for x in "Python":
    print(x)
print("Done", newLine)

print(" - - - Iterate List - - - ")
# list is iterables
for x in [1, 2, 3, 4]:
    print(x)
print("Done", newLine)

print(" - - - While Loop - - - ")
# while loop
# 100
# 50
# 25
# 12
# 6
# 3
# 1
num = 100
while num > 0:
    print(num)
    num //= 2  # integer division
print("Done", newLine)

print(" - - - Infinite While Loop - - - ")
# infinite loop
# beware of breaking from the loop
# 100
# 50
# 25
# 12
# 6
num = 100
while True:
    print(num)
    num //= 2
    if num == 3:
        break
print("Completed!", newLine)

print(" - - - Print Even Numbers - - - ")
# print even numbers
# 2
# 4
# 6
# 8
counter = 0
for x in range(1, 10):
    if x % 2 == 0:
        print(x)
        counter += 1
print(
    f"We have {counter} even {'numbers' if counter > 1 else 'number'}", newLine)
