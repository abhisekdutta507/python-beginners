# define function with 2 parameters


def greet(first_name, last_name):
    # formatted string
    print(f"Hi {first_name.title()} {last_name.title()}")
    print("Welcome aboard")


# passing argument to greet()
# keyword argument
greet("abhisek", last_name="dutta")


def get_greeting(name):
    return f"Welcome {name.title()}"


# keyword argument
message = get_greeting(name="abhisek dutta")
print(message)


def increment(number, by):
    return number + by


# keyword argument
print(increment(number=5, by=2))


def multiply(number, by=1):  # default argument
    return number * by


# keyword argument
print(multiply(number=5))


def sum(*numbers):  # variable arguments as tupples
    total = 0
    for x in numbers:
        total += x
    # return statement always return from the block of code or function
    # formatted string
    return f"summation of {numbers} is {total}"


print(sum(1, 4, 5, 6))


def save_user(**user):  # pass dictionary as a function parameter
    print(user)
    print(user["name"])


save_user(id=1, name="Abhisek")

message = "Welcome"


def greet_message():
    # local variable
    message = "Hello"
    print(message)


greet_message()
print(message)


def override_global_message():
    global message
    message = "Hurray"


override_global_message()
print(message)
